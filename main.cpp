// CS3241Lab1.cpp : Defines the entry point for the console application.
//#include <cmath>
#include "math.h"
#include <iostream>
#include <fstream>
//gl headers for xcode
#include <OpenGL/gl.h>
#include <GLUT/GLUT.h>

// global variable
// pi value can be accessed using the built-in M_PI
bool m_Smooth = true;
bool m_Highlight = true;
bool m_Emission = false;
GLfloat angle = 0;   /* in degrees */
GLfloat angle2 = 0;   /* in degrees */
GLfloat zoom = 1.0;
int mouseButton = 0;
int moving, startx, starty;

float white[] = {1.0f, 1.0f, 1.0f, 1.0f};
float black[] = {0.0f, 0.0f, 0.0f, 1.0f};
float blue[] = {0.1f, 0.5f, 0.8f, 1.0f};
float red[] = {0.8f, 0.1f, 0.3f, 1.0f};
float orange[] = {0.8f, 0.35f, 0.1f, 1.0f};
float yellow_orange[] = {0.8f, 0.5f, 0.1f, 1.0f};
float green[] = {0.1f, 1.0f, 0.3f, 1.0f};

int current_object = 2;

using namespace std;

void addLight(GLenum light, GLfloat lightPosition[], GLfloat intensity, GLfloat specularIntensity)
{
    GLfloat ambientIntensity = 0.01;
    GLfloat	ambientProperties[]  = {ambientIntensity, ambientIntensity, ambientIntensity, 1.0f};
	GLfloat	diffuseProperties[]  = {intensity, intensity, intensity, 1.0f};
    GLfloat	specularProperties[] = {specularIntensity, specularIntensity, specularIntensity, 1.0f};

	glLightfv(light, GL_POSITION, lightPosition);
	
    glLightfv(light, GL_AMBIENT, ambientProperties);
    glLightfv(light, GL_DIFFUSE, diffuseProperties);
    glLightfv(light, GL_SPECULAR, specularProperties);
    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 0.0);
	glEnable(light);
}

void setupLighting()
{
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
    glClearDepth(1.0);
    
	GLfloat keyLightPosition[] = {-100.0f,50.0f,120.0f,1.0f};
    addLight(GL_LIGHT0, keyLightPosition, 0.8, 0.8);
    
    GLfloat fillLightPosition[] = {200.0f,100.0f,220.0f,1.0f};
    addLight(GL_LIGHT1, fillLightPosition, 0.3, 0.1);
    
    GLfloat backLightPosition[] = {-150.0f,100.0f,-220.0f,1.0f};
    addLight(GL_LIGHT2, backLightPosition, 0.3, 0.1);
    
	// Default : lighting
	glEnable(GL_LIGHTING);
    
}

void setMaterial(float mat_diffuse[], GLfloat shininess)
{
    float no_mat[] = {0.0f, 0.0f, 0.0f, 1.0f};
    float mat_ambient[] = {0.3f, 0.3f, 0.3f, 1.0f};
    float mat_ambient_color[] = {0.8f, 0.8f, 0.2f, 1.0f};
    float no_shininess = 0.0f;
    float mat_emission[] = {0.9f, 0.2f, 0.9f, 1.0f};
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    
	if(m_Highlight)
	{
		// your codes for highlight here
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat_ambient_color);
		glMaterialf(GL_FRONT, GL_SHININESS, shininess);
    } else {
		glMaterialfv(GL_FRONT, GL_SPECULAR, no_mat);
		glMaterialf(GL_FRONT, GL_SHININESS, no_shininess);
	}
    
    if(m_Emission)
    {
        glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);
	} else {
        glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
    }
    
    glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
}

void drawSphereVertex(double r, int n, int i, int j)
{
    glVertex3d(r*sin(i*M_PI/n)*cos(j*M_PI/n),r*cos(i*M_PI/n)*cos(j*M_PI/n),r*sin(j*M_PI/n));
}

void drawSphereNormal(int n, GLfloat i, GLfloat j)
{
    glNormal3d(sin(i*M_PI/n)*cos(j*M_PI/n),cos(i*M_PI/n)*cos(j*M_PI/n),sin(j*M_PI/n));
}

void drawSphere(double r, int n, float mat_diffuse[])
{
    setMaterial(mat_diffuse, 10.0);
	int i,j;
    for(i=0;i<n;i++)
    {
		for(j=0;j<2*n;j++)
        {
            if(m_Smooth)
            {
                glBegin(GL_POLYGON);
                drawSphereNormal(n, i, j);
                drawSphereVertex(r, n, i, j);
                
                drawSphereNormal(n, i+1, j);
                drawSphereVertex(r, n, i+1, j);
                
                drawSphereNormal(n, i+1, j+1);
                drawSphereVertex(r, n, i+1, j+1);
                
                drawSphereNormal(n, i, j+1);
                drawSphereVertex(r, n, i, j+1);
                glEnd();
            } else	{
                glBegin(GL_POLYGON);
                drawSphereNormal(n, i+0.5, j+0.5);
                drawSphereVertex(r, n, i, j);
                drawSphereVertex(r, n, i + 1, j);
                drawSphereVertex(r, n, i + 1, j + 1);
                drawSphereVertex(r, n, i, j + 1);
                glEnd();
            }
        }
    }
}


void drawSphereSection(double r, int n, float mat_diffuse[], int limit)
{
    setMaterial(mat_diffuse, 10.0);
	int i,j;
    for(i=0;i<limit;i++)
    {
		for(j=n/2;j<1.5*n;j++)
        {
            if(m_Smooth)
            {
                glBegin(GL_POLYGON);
                drawSphereNormal(n, i, j);
                drawSphereVertex(r, n, i, j);
                
                drawSphereNormal(n, i+1, j);
                drawSphereVertex(r, n, i+1, j);
                
                drawSphereNormal(n, i+1, j+1);
                drawSphereVertex(r, n, i+1, j+1);
                
                drawSphereNormal(n, i, j+1);
                drawSphereVertex(r, n, i, j+1);
                glEnd();
            } else	{
                glBegin(GL_POLYGON);
                drawSphereNormal(n, i+0.5, j+0.5);
                drawSphereVertex(r, n, i, j);
                drawSphereVertex(r, n, i + 1, j);
                drawSphereVertex(r, n, i + 1, j + 1);
                drawSphereVertex(r, n, i, j + 1);
                glEnd();
            }
        }
    }
}

void drawCylinderPoint(GLfloat radius, GLfloat i, GLfloat sides, GLfloat height)
{
    double x = radius * sin(2.0*M_PI*i/sides);
    double z = radius * cos(2.0*M_PI*i/sides);
    glVertex3d(x, height, z);
}

void drawCylinderNormal(GLfloat radius, GLfloat i, GLfloat sides)
{
    double x = radius * sin(2.0*M_PI*i/sides);
    double z = radius * cos(2.0*M_PI*i/sides);
    glNormal3d(x, 0, z);
}

void drawCylinder(GLfloat r, GLfloat h, bool isCapped, int sides, float mat_diffuse[])
{
    setMaterial(mat_diffuse, 10.0);
    for(int i = 0; i < sides; i++)
    {
        if(m_Smooth)
        {
            glBegin(GL_POLYGON);
            drawCylinderNormal(r, i, sides);
            drawCylinderPoint(r, i, sides, h);
            
            drawCylinderNormal(r, i+1, sides);
            drawCylinderPoint(r, i+1, sides, h);
            
            drawCylinderNormal(r, i+1, sides);
            drawCylinderPoint(r, i+1, sides, 0);
            
            drawCylinderNormal(r, i, sides);
            drawCylinderPoint(r, i, sides, 0);
            glEnd();
        } else	{
            glBegin(GL_POLYGON);
            drawCylinderNormal(r, i, sides);
            drawCylinderPoint(r, i, sides, h);
            drawCylinderPoint(r, i+1, sides, h);
            drawCylinderPoint(r, i+1, sides, 0);
            drawCylinderPoint(r, i, sides, 0);
            glEnd();
        }
    }
    if(isCapped)
    {
        glBegin(GL_POLYGON);
        glNormal3d(0, 1, 0);
        for(int i = 0; i < sides; i++)
        {
            drawCylinderPoint(r, i, sides, h);
        }
        glEnd();
        glBegin(GL_POLYGON);
        glNormal3d(0, -1, 0);
        for(int i = 0; i < sides; i++)
        {
            drawCylinderPoint(r, i, sides, 0);
        }
        glEnd();
    }
}

void drawTubeCover(GLfloat innerRadius, GLfloat outerRadius, int sides, GLfloat height)
{
    for(int i = 0; i < sides; i++)
    {
        glBegin(GL_POLYGON);
        drawCylinderPoint(outerRadius, i, sides, height);
        drawCylinderPoint(outerRadius, i+1, sides, height);
        drawCylinderPoint(innerRadius, i+1, sides, height);
        drawCylinderPoint(innerRadius, i, sides, height);
        glEnd();
    }
}

void drawTube(GLfloat innerRadius, GLfloat outerRadius, GLfloat height, int sides, float mat_diffuse[])
{
    drawCylinder(innerRadius, height, false, sides, mat_diffuse);
    drawCylinder(outerRadius, height, false, sides, mat_diffuse);
    
    setMaterial(mat_diffuse, 10.0);
    glNormal3d(0, 1, 0);
    drawTubeCover(innerRadius, outerRadius, sides, height);
    glNormal3d(0, -1, 0);
    drawTubeCover(innerRadius, outerRadius, sides, 0);
}

void drawSprialSection(int n)
{
    if(n == 0) {
        return;
    }
    
    glPushMatrix();
        drawCylinder(0.05, 0.8, true, 8, blue);
        glTranslatef(0, 0.8, 0);
        drawSphere(0.07, 10, blue);
        glScalef(0.95, 0.95, 0.95);
        glRotatef(20, 0, 0, 1);
        glRotatef(5, 0, 1, 0);
        drawSprialSection(n-1);
    glPopMatrix();
}

void drawSpiral()
{
    glPushMatrix();
        glScalef(0.5, 0.5, 0.5);
        drawSprialSection(60);
    glPopMatrix();
}

void drawHemisphere(double r, int n, float mat_diffuse[])
{
    setMaterial(mat_diffuse, 10.0);
	int i,j;
    for(i=0;i<n;i++)
    {
		for(j=0;j<n;j++)
        {
            if(m_Smooth)
            {
                glBegin(GL_POLYGON);
                drawSphereNormal(n, i, j);
                drawSphereVertex(r, n, i, j);
                
                drawSphereNormal(n, i+1, j);
                drawSphereVertex(r, n, i+1, j);
                
                drawSphereNormal(n, i+1, j+1);
                drawSphereVertex(r, n, i+1, j+1);
                
                drawSphereNormal(n, i, j+1);
                drawSphereVertex(r, n, i, j+1);
                glEnd();
            } else	{
                glBegin(GL_POLYGON);
                drawSphereNormal(n, i+0.5, j+0.5);
                drawSphereVertex(r, n, i, j);
                drawSphereVertex(r, n, i + 1, j);
                drawSphereVertex(r, n, i + 1, j + 1);
                drawSphereVertex(r, n, i, j + 1);
                glEnd();
            }
        }
    }
}


void drawTubeCoverSection(GLfloat innerRadius, GLfloat outerRadius, int sides, int limit, GLfloat height)
{
    for(int i = 0; i < limit; i++)
    {
        glBegin(GL_POLYGON);
        drawCylinderPoint(outerRadius, i, sides, height);
        drawCylinderPoint(outerRadius, i+1, sides, height);
        drawCylinderPoint(innerRadius, i+1, sides, height);
        drawCylinderPoint(innerRadius, i, sides, height);
        glEnd();
    }
}


void drawCylinderSection(GLfloat r, GLfloat h, bool isCapped, int sides, int limit, float mat_diffuse[])
{
    setMaterial(mat_diffuse, 10.0);
    for(int i = 0; i < limit; i++)
    {
        if(m_Smooth)
        {
            glBegin(GL_POLYGON);
            drawCylinderNormal(r, i, sides);
            drawCylinderPoint(r, i, sides, h);
            
            drawCylinderNormal(r, i+1, sides);
            drawCylinderPoint(r, i+1, sides, h);
            
            drawCylinderNormal(r, i+1, sides);
            drawCylinderPoint(r, i+1, sides, 0);
            
            drawCylinderNormal(r, i, sides);
            drawCylinderPoint(r, i, sides, 0);
            glEnd();
        } else	{
            glBegin(GL_POLYGON);
            drawCylinderNormal(r, i, sides);
            drawCylinderPoint(r, i, sides, h);
            drawCylinderPoint(r, i+1, sides, h);
            drawCylinderPoint(r, i+1, sides, 0);
            drawCylinderPoint(r, i, sides, 0);
            glEnd();
        }
    }
    if(isCapped)
    {
        glBegin(GL_POLYGON);
        glNormal3d(0, 1, 0);
        for(int i = 0; i < limit+1; i++)
        {
            drawCylinderPoint(r, i, sides, h);
        }
        glEnd();
        glBegin(GL_POLYGON);
        glNormal3d(0, -1, 0);
        for(int i = 0; i < limit+1; i++)
        {
            drawCylinderPoint(r, i, sides, 0);
        }
        glEnd();
    }
}



void drawTubeSection(GLfloat innerRadius, GLfloat outerRadius, GLfloat height, int sides, int limit, float mat_diffuse[])
{
    drawCylinderSection(innerRadius, height, false, sides, limit, mat_diffuse);
    drawCylinderSection(outerRadius, height, false, sides, limit, mat_diffuse);
    
    setMaterial(mat_diffuse, 10.0);
    glNormal3d(0, 1, 0);
    drawTubeCoverSection(innerRadius, outerRadius, sides, limit, height);
    glNormal3d(0, -1, 0);
    drawTubeCoverSection(innerRadius, outerRadius, sides, limit, 0);
}

void drawRectangle(GLfloat width, GLfloat length)
{
    glNormal3d(0, 1, 0);
    glBegin(GL_POLYGON);
        glVertex3f(0, 0, 0);
        glVertex3f(width, 0, 0);
        glVertex3f(width, 0, length);
        glVertex3f(0, 0, length);
    glEnd();
}

void drawOrange()
{
    int sides = 20;
    GLfloat outerRadius = 1.0;
    GLfloat innerRadius = 0.9;
    glPushMatrix();
        glRotatef(90, 1, 0, 0);
        drawHemisphere(outerRadius, sides, orange);
        drawHemisphere(innerRadius, sides, orange);
    glPopMatrix();
    drawTubeCover(innerRadius, outerRadius, sides * 2, 0);
    setMaterial(white, 10.0);
    glPushMatrix();
        for(int i = 0; i < 10; i++)
        {
            glRotatef(36, 0, 1, 0);
            glPushMatrix();
                glTranslatef(0, 0, 0.1);
                drawRectangle(0.015, 0.8);
            glPopMatrix();
        }
    glPopMatrix();
    glPushMatrix();
        glTranslatef(0, -0.05, 0);
        drawTube(0.83, 0.9, 0.05, 40, white);
        glPushMatrix();
            glTranslatef(0, 0.04, 0);
            drawTube(0.09, 0.1, 0.01, 40, white);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0, -0.9, 0);
            drawTube(0.09, 0.1, 0.94, 40, yellow_orange);
        glPopMatrix();
        glTranslatef(0, -0.0001, 0);
        drawTube(0.101, outerRadius-0.05, 0.05, 40, yellow_orange);
    glPopMatrix();
}

void drawSeeds()
{
    glPushMatrix();
    glRotatef(-89, 0, 1, 0);
    for(int i = 0; i < 11; i++)
    {
        glRotatef(15, 0, 1, 0);
        glPushMatrix();
            glTranslatef(0.4, 0, 0);
            glPushMatrix();
                glScalef(1.0, 0.3, 0.7);
                drawSphere(0.03, 8, black);
            glPopMatrix();
        glPopMatrix();
    }
    glPopMatrix();
}

void drawWatermelon()
{
    GLfloat pulpCylinderThickness = 0.001;
    glPushMatrix();
        glRotatef(17, 0, 0, 1);
        drawSphereSection(1, 20, green, 4);
        glRotatef(-90, 0, 0, 1);
        setMaterial(white, 10.0);
        drawTubeCoverSection(0.95, 1, 40, 20, 0);
        glPushMatrix();
            glTranslatef(0, -pulpCylinderThickness, 0);
            drawCylinderSection(0.95, pulpCylinderThickness, true, 40, 20, red);
        glPopMatrix();
        drawSeeds();
        glRotatef(-36, 0, 0, 1);
        setMaterial(white, 10.0);
        drawTubeCoverSection(0.95, 1, 40, 20, 0);
        glPushMatrix();
            glTranslatef(0, pulpCylinderThickness, 0);
            drawCylinderSection(0.95, pulpCylinderThickness, true, 40, 20, red);
        glPopMatrix();
        drawSeeds();
    glPopMatrix();
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
    glTranslatef(0, 0, -6);
    
    glRotatef(angle2, 1.0, 0.0, 0.0);
    glRotatef(angle, 0.0, 1.0, 0.0);
    
    glScalef(zoom,zoom,zoom);
    
    switch (current_object) {
		case 0:
			drawSphere(1, 20, orange);
			break;
		case 1:
            drawTube(0.4, 0.7, 0.2, 30, green);
			break;
		case 2:
            drawOrange();
			break;
		case 3:
            drawWatermelon();
			break;
        case 4:
            drawCylinder(0.5,1, true, 20, blue);
			break;
        case 5:
            break;
		default:
			break;
    };
	glPopMatrix();
	glutSwapBuffers ();
}

void keyboard (unsigned char key, int x, int y)
{
	switch (key) {
        case 'p':
        case 'P':
            glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
            break;
        case 'w':
        case 'W':
            glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
            break;
        case 'v':
        case 'V':
            glPolygonMode(GL_FRONT_AND_BACK,GL_POINT);
            break;
        case 's':
        case 'S':
            m_Smooth = !m_Smooth;
            break;
        case 'h':
        case 'H':
            m_Highlight = !m_Highlight;
            break;
            
        case 'e':
        case 'E':
            m_Emission = !m_Emission;
            break;
            
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            current_object = key - '1';
            break;
            
        case 'Q':
        case 'q':
            exit(0);
            break;
            
        default:
            break;
	}
    
	glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN) {
        mouseButton = button;
        moving = 1;
        startx = x;
        starty = y;
    }
    if (state == GLUT_UP) {
        mouseButton = button;
        moving = 0;
    }
}

void motion(int x, int y)
{
    if (moving) {
        if(mouseButton==GLUT_LEFT_BUTTON)
        {
            angle = angle + (x - startx);
            angle2 = angle2 + (y - starty);
        }
        else zoom += ((y-starty)*0.001);
        startx = x;
        starty = y;
        glutPostRedisplay();
    }
    
}

int main(int argc, char **argv)
{
	cout<<"CS3241 Lab 3"<< endl<< endl;
    
	cout << "1-4: Draw different objects"<<endl;
	cout << "S: Toggle Smooth Shading"<<endl;
	cout << "H: Toggle Highlight"<<endl;
	cout << "W: Draw Wireframe"<<endl;
	cout << "P: Draw Polygon"<<endl;
	cout << "V: Draw Vertices"<<endl;
	cout << "Q: Quit" <<endl<< endl;
    
	cout << "Left mouse click and drag: rotate the object"<<endl;
	cout << "Right mouse click and drag: zooming"<<endl;
    
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (50, 50);
	glutCreateWindow ("CS3241 Assignment 3");
	glClearColor (1.0,1.0,1.0, 1.0);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	setupLighting();
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
    
    glMatrixMode(GL_PROJECTION);
    gluPerspective( /* field of view in degree */ 40.0,
                   /* aspect ratio */ 1.0,
                   /* Z near */ 1.0, /* Z far */ 80.0);
	glMatrixMode(GL_MODELVIEW);
	glutMainLoop();
    
	return 0;
}